const config = require('../../config/config');
const logger = require('tracer').console();
const request = require('request');
const db = require('../dbConnection');
const notificationService = require('../notificationService')

const APP_TYPE = {
    HTTP: 'http',
    SQL: 'sql'
};
const APP_STATUS = {
    OK: "ok",
    UNHEALTHY: "unhealthy",
    PENDING: "pending"
};

var listOfAlerts = {};
function HttpCheck(alertId, params){
    var callConfig = {
        method: params.appConfig.method,
        url: params.appConfig.url,
        headers: params.appConfig.headers,
        body: params.appConfig.data
    };
    request(callConfig,function (err,response,body) {
        var dbo = db.mongodbConn.db(config.mongo.db);

        if(err || response.statusCode>399) {
            logger.info("health check failed");
            var currentState = APP_STATUS.UNHEALTHY
            //send notification
        }
        else {
            var currentState = APP_STATUS.OK
        }
        dbo.collection("status_log")
            .insertOne({
                alertId: alertId,
                state: currentState,
                checkedAt: Date.now(),
                remarks: {
                    responseStatus: response.statusCode,
                    responseBody: body.substr(0,500),   //max limit 500bytes, to avoid cluttering of data
                    errMessage: err
                }
            });
        //decide action on currentStatus
        if(listOfAlerts[alertId].lastState != currentState){
            if(currentState==APP_STATUS.OK){
                //close the open incident
                dbo.collection("incident")
                    .updateOne({
                        alertId: alertId,
                        incidentState: "open",
                    },{
                        $set : {
                            resolvedAt: Date.now(),
                            incidentState: "resolved",
                            lastUpdated: Date.now(),
                        }
                    })
            }
            else {
                //open an incident
                var incident = dbo.collection("incident")
                    .insertOne({
                        alertId: alertId,
                        state: currentState,
                        incidentState: "open",
                        userAction: null,
                        createdAt: Date.now(),
                        resolvedAt: null,
                        lastUpdated: Date.now(),
                        notifiedTo: {},
                        remarks: {}
                    }).then(function(result){
                        return result.insertedId
                    });
                //notify via email/slack/sms
                var incidentId = Promise.all(incident)
                params.notificationChannel.forEach((target,index)=>{
                    notificationService.notifyIncident(target.channel,{
                        address: target.address,
                        alertId: alertId,
                        incidentId: incidentId[0]
                    })
                })
            }
            listOfAlerts[alertId].lastState = currentState
        }
    })

}

function SQLCheck(alertId, params){
    logger.info(params)
}//(url,username,password,query){}


module.exports = {
    startMonitoring: function (alertParams,next) {
        if(alertParams==null)
            next("Invalid Parameters",null);
        if(alertParams.appType==APP_TYPE.HTTP) {
            var execute = HttpCheck;
        }
        else if(alertParams.appType==APP_TYPE.SQL)
            var execute = SQLCheck;
        else next("Invalid appType",null);
        var timeObject = setInterval(execute,alertParams.frequency,alertParams.id,alertParams);
        listOfAlerts[alertParams.id] = {};
        listOfAlerts[alertParams.id].reference = timeObject;
        listOfAlerts[alertParams.id].lastState = APP_STATUS.PENDING;
        next(null,"monitoring started")
    },
    stopMonitoring: function (alertId,next) {
        if(listOfAlerts[alertId]!=null)
            clearInterval(listOfAlerts[alertId].reference);
        next(null,"alert disabled")
    },

};