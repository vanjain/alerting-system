const mongo = require('mongodb');
const config = require('../config/config');

const mongoClient = mongo.MongoClient;
mongoClient.connect(config.mongo.url,{poolSize: config.mongo.poolSize},function(err,db){
    if(err)
        throw err;
    exports.mongodbConn = db;

});
