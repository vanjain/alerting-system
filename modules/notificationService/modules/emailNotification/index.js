'use strict';
const config = require('../../../../config/config');
const nodemailer = require('nodemailer');
const logger = require('tracer').console();

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport({
    host: config.mail.host,
    port: config.mail.port,
    secure: true, // true for 465, false for other ports
    auth: {
        user: config.mail.user,
        pass: config.mail.pass
    }
});
module.exports= {
        sendMail : function (emailInfo) {
            //transform emailInfo to mailOptions
            var mailOptions=emailInfo;
            mailOptions.from='"TestMail Service "<'+config.mail.user+'>'; // sender address
                transporter.sendMail(mailOptions, function(error, info) {
                if (error) {
                    logger.error(error);
                    return false;
                }
                logger.info('Message sent: %s', info.messageId);
                return true;
            });

        }
};
