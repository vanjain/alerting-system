const config = require('../../config/config');
const email=require('./modules/emailNotification/');
const sms=require('./modules/smsNotification/')();
const slack = require('./modules/slackNotification')();

module.exports = {
        emailSend : function () {
            const mailOptions = {
                to: 'vanjain18@gmail.com', // list of receivers
                subject: 'Hello ✔', // Subject line
                text: 'Hello world?', // plain text body
                html: '<b>Hello world?</b>' // html body
            };
            console.log("here emailSend");
            return email.sendMail(mailOptions)
        },
        notifyIncident : function (channel,parameters) {
            switch (channel) {
                case "email" :
                    var mailOptions = {
                        to: parameters.address,
                        subject: parameters.incidentId+': An Incident has been opened against alert '+parameters.alertId,
                        text: 'Please address the incident'
                    };
                    email.sendMail(mailOptions);
                    break;
                case "slack":
                    //slack.notify()
                    break;
                case "sms":
                    //sms.send()
                    break;
                default:
            }
            return
        }
}
