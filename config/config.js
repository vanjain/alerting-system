var _ = require('lodash');

// Extend the base configuration in all.js with environment
// specific configuration
var environment = process.env.ENVIRONMENT || 'dev';
module.exports = _.merge(
    require(__dirname + '/../config/env/all.js'),
    require(__dirname + '/../config/env/' + environment + '.js') || {}
);
