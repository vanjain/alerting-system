
module.exports = {
    swagger:{
        info: {
            version: '1.0.0',
            title: 'Alerting System',
        },
        security: {
            BasicAuth: {
                type: 'http',
                scheme: 'basic',
            },
        },
        filesPattern: '../../**/*.js', // Glob pattern to find your jsdoc files
        swaggerUIPath: '/swagger', // SwaggerUI will be render in this url. Default: '/api-docs'
        baseDir: __dirname,
    },
    mongo: {
        url: 'mongodb://127.0.0.1:27017/af_alert_system',
        db: 'af_alert_system',
        poolSize: 10
    },
    mail:{
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, // true for 465, false for other ports
        user: "xxxx@gmail.com",
        pass: ""
    }
};