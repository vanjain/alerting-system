var express = require('express');
var router = express.Router();
var notify = require('../modules/notificationService/index');


/* home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/**
 *  POST /email
 *  @summary sends test email
 *  */
router.post('/email',function (req,res,next) {
  res.send(notify.emailSend())
});

//list all routing here
router.use('/users', require('./users'));
router.use('/v1/alerts/',require('./alerts'));

module.exports = router;
