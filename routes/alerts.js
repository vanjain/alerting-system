var express = require('express');
var router = express.Router();
var logger = require('tracer').console();
var config = require('../config/config');
var monitor = require('../modules/monitoringService');
var db = require('../modules/dbConnection');

router.get("/",function (req, res, next) {
    res.render('index', { title: 'Alerts' });
});

/**
 * @typedef {object} AlertConfigs
 * @property {string} name - required - Name of Alert
 * @property {string} appType - Type of Application, HTTP/SQL/Redis - enum:http,sql,redis
 * @property {number} frequency - Frequency of Checks
 * @property {string} frequencyUnit - Unit of frequency seconds/minutes - enum:milliseconds
 * @property {oneOf|HttpConfig|SQLConfig} appConfig - application configs
 * @property {array<NotificationChannel>} notificationChannel - Where notification has to be sent
 */

/**
 * @typedef {object} HttpConfig
 * @property {string} method.required - http method to use - enum: GET,POST,PUT
 * @property {string} url.required
 * @property {object} headers
 * @property {object} data
 */

/**
 * @typedef {object} SQLConfig
 * @property {string} url.required
 * @property {string} username.required
 * @property {string} password.required
 * @property {string} query.required
 */

/**
 * @typedef {object} NotificationChannel
 * @property {string} channel.required - channel - enum:slack,email,sms
 * @property {string} address - target address - emailId,phoneNumber,slackChannel
 */

/**
 * @typedef {object} AddAlertResponse
 * @property {string} id - alertId
 */

/**
 * POST /v1/alerts/add
 * @tags Alerts
 * @summary Endpoint to add an alert for a given application
 * @param {AlertConfigs} request.body - alert configs
 * @return {AddAlertResponse} 200 - success response with created alert Id
 * @return {object} 400 - bad request/invalid format
 * @return {object} 500 - server error
 */
router.post("/add",function (req, res, next) {
    //validate input req.body
    var params = req.body;
    //add to db
    var dbo = db.mongodbConn.db(config.mongo.db);
    var promise = dbo.collection("alerts")
        .insertOne(params);

    promise.then(function (result) {
        logger.info(result);
        params.id = result.insertedId.toString();
        //start monitoring alert
        monitor.startMonitoring(params,function (err,result) {
            if(err)
                res.status(500);
            else {
                var output = { id: params._id}
                res.send(output)
            }
        });
    }, function (err) {
        res.status(500);
    });
});

/**
 * PUT /v1/alerts/edit/{alertId}
 * @tags Alerts
 * @summary Endpoint to edit an alert for a given application
 * @param {string} alertId.path.required - alert id required
 * @return {object} 200 - success response
 */
router.put("/edit/:alertId",function (req, res, next) {
    res.render('index', { title: 'Alerts' });
});

/**
 * GET /v1/alerts/status/{alertId}
 * @tags Alerts
 * @summary Endpoint to get the current status of an alert
 * @param {string} alertId.path.required - alert id required
 * @return {object} 200 - success response
 */
router.get("/status/:alertId",function (req, res, next) {
    var dbo = db.mongodbConn.db(config.mongo.db);
    dbo.collection("status_log")
        .find({ alertId: req.params.alertId})
        .sort({checkedAt:-1})
        .limit(10)
        .toArray(function (err,result) {
            if(err)
                res.status(500);
            else res.send(result);
        });
});

/**
 * GET /v1/alerts/list/
 * @tags Alerts
 * @summary Endpoint to get the current status of all alerts
 * @return {object} 200 - success response
 */
router.get("/list",function (req, res, next) {
    var dbo = db.mongodbConn.db(config.mongo.db);
    dbo.collection("alerts")
        .find({},{_id:true,name:true})
        .toArray(function (err,result) {
            if(err)
                res.status(500);
            else res.send(result);
        });
});

/**
 * POST /v1/alerts/load-all/
 * @tags Bootstrap
 * @summary Endpoint to load all alerts
 * @return {object} 200 - success response
 */

router.post("/load-all",function (req,res,next) {
    //bootstrap all alerts
    db.mongodbConn
        .db(config.mongo.db)
        .collection("alerts")
        .find({})
        .toArray(function (err,result) {
            if(!err)
                result.forEach((alert,index)=>{
                    alert.id=alert._id.toString();
                    monitor.startMonitoring(alert,function (err,d) {
                        logger.info(alert._id +" started")
                    })
                });
            res.send()
        });
});

module.exports = router;