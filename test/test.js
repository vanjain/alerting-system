const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../bin/www');
const app = require('../app');

chai.use(chaiHttp);
const expect = chai.expect;

describe('POST /alerts',()=> {
    it('should create an alert',(done)=>{
        var request = {
            "name": "tt1",
            "appType": "http",
            "frequency": 10000,
            "frequencyUnit": "millisecond",
            "appConfig": {
                "method":"GET",
                "url":"http://localhost:3000",
                "headers": {},
                "body": {}
            },
            "notificationChannel": [
                {
                    "channel": "email",
                    "address": "abc@asa.com"
                }
            ]
        };
        chai.request(app)
            .post('/v1/alerts/add')
            .send(request)
            .end(((err, res) => {
                //asserts
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res).json.to.be.not.null;
                done();
            }));

    });

    it('should return invalid request error',(done)=>{
        chai.request(app)
            .post('/v1/alerts/add')
            .send({})
            .end(((err, res) => {
                //asserts
                expect(err).to.be.null;
                expect(res).to.have.status(400);
                done();
            }));
    });

    it('should return 500 error', (done)=>{
        done();
    });
});