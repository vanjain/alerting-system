Build Service

sudo docker build -t alerting-system .

sudo docker run -p 49160:8080 -e PORT=8080 -d alerting-system

#For running mongo db
sudo docker run -it -p 27017:27017  --name mongodb -d mongo 